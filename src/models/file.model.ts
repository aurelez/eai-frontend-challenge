export class FileModel {
    name: string;
    path: string;
    content: string;
    createdAt: string;
    updatedAt: string;

    constructor(file: any) {
        this.name = file.name;
        this.path = file.path;
        this.content = file.content;
        this.createdAt = file.createdAt;
        this.updatedAt = file.updatedAt;
    }
}