import { FileModel } from './file.model';
export class NodeModel {
    name: string;
    file: FileModel;
    children : NodeModel[];

    constructor(name: string, children: NodeModel[], file: FileModel) {
        this.name = name;
        this.file = file;
        if (this.file) this.file.name = name;
        this.children = children;
    }
}