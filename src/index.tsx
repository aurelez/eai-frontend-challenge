import React from 'react';
import ReactDOM from 'react-dom';
import './index.scss';
import AppComponent from './components/App/app.component';

ReactDOM.render(<AppComponent />, document.getElementById('root'));
