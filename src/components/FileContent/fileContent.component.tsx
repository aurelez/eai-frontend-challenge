import React from "react";
import "./fileContent.component.scss";
import { fileService } from "../../services/file.service"
import { FileModel } from "../../models/file.model"
import { Subscription } from "rxjs";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faFolder, faFileAlt } from '@fortawesome/free-solid-svg-icons'

interface FileProps {}
interface FileState {
  file: FileModel,
  isVisible: boolean
}


class FileContentComponent extends React.Component<FileProps, FileState> {
  
  subscription: Subscription;

  constructor(props: {}) {
    super(props);
    this.state = { file: new FileModel({}), isVisible: false };
    this.subscription = new Subscription();
  }

  componentDidMount() {
    this.subscription = fileService.getFile().subscribe( file => {
      this.setState({file: file as FileModel, isVisible: true});
    });
  }
  
  componentWillUnmount() {
    this.subscription.unsubscribe();
  }

  render() {
    return (
      <div className="FileContent">
        <div className="help box">
          <p>&#9755; Click on <FontAwesomeIcon icon={faFolder} /> folder to toogle it (open/close)</p>
          <p>&#9755; Click on <FontAwesomeIcon icon={faFileAlt} /> file to display his content</p>
        </div>
        {
          this.state.isVisible &&
          <div className="box file">
            <h3>{this.state.file.name}</h3>
            <div className="meta">
              <p><b>Full path:</b> {this.state.file.path}</p>
              <p><b>Created at:</b> {this.state.file.createdAt}</p>
              <p><b>updated at:</b> {this.state.file.updatedAt}</p>
            </div>
            <br/>
            <p>{this.state.file.content}</p> 
          </div>   
        }  
      </div>
    );
  }
}

export default FileContentComponent;
