import React from "react";
import "./fileTree.component.scss";
import { fileService } from "../../services/file.service"
import { NodeModel } from "../../models/node.model";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faFolder, faFolderOpen, faFileAlt } from '@fortawesome/free-solid-svg-icons'

interface FileTreeProps {
  node: NodeModel,
}

interface FileTreeState {
  icon: any,
  isOpned: boolean
}

class FileTreeComponent extends React.Component<FileTreeProps, FileTreeState> {
  
  constructor(props: FileTreeProps) {
    super(props);
    this.state = { 
      isOpned: this.isRoot() ? true : false,
      icon: this.getNodeIcon()
    }
  }

  onNodeClick = () => {
    if (this.props.node.file) {
      fileService.sendFile(this.props.node.file);
    } else {
      this.setState({
        isOpned: !this.state.isOpned,
        icon: this.getNodeIcon()
      });
    }
  }

  getNodeIcon = () => {
    if (this.props.node.file) {
      return faFileAlt;
    } else {
      return !this.state
        ?  (this.isRoot()) ? faFolderOpen : faFolder
        : this.state.isOpned ? faFolder : faFolderOpen;
    }
  }

  isRoot = () : boolean => this.props.node.name === 'root'

  render() {
    let { node } = this.props;

    return (
      <div className="wrap_file">
        <div className="filename" onClick={this.onNodeClick}>
          <FontAwesomeIcon icon={this.state.icon} />&nbsp;
          <span>{node.name}</span>
        </div>
        { 
          this.state.isOpned &&
          <ul>
            {node.children.map((nodeChild, index) => {
              return <li key={index}><FileTreeComponent node={nodeChild} /></li>
            })}
          </ul>
        } 
      </div>
    );
  }
}

export default FileTreeComponent;
