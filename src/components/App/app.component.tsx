import React from "react";
import "./app.component.scss";
import FileTreeComponent from "../FileTree/fileTree.component";
import FileContentComponent from "../FileContent/fileContent.component";
import { FileTreeService } from "../../services/filetree.service";
import { NodeModel } from "../../models/node.model";

class AppComponent extends React.Component {

  fileTreeService: FileTreeService;
  fileTreeObject: NodeModel;

  constructor(props: {}) {
    super(props);
    this.fileTreeService = new FileTreeService();
    try {
      this.fileTreeObject = this.fileTreeService.getFileTree();
      console.log(this.fileTreeObject);
    } catch (e) { console.error("Error with the filetree source format: ", e) }
    
  }
  
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <h4>ElementAI Frontend Challenge</h4>
          <span><b>Candidate:</b>&nbsp;
          <a href="https://www.linkedin.com/in/aurele/">Aurèle Zannou</a></span>
        </header>
        <div className="App-content">
          <div className="FileTree">
            <FileTreeComponent node={this.fileTreeObject} />
          </div>
          <FileContentComponent />
        </div>
      </div>
    );
  }
};

export default AppComponent;
