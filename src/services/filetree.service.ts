import { NodeModel } from './../models/node.model';
import { FileModel } from './../models/file.model';
import fileFixtures from "../fixtures/files.json";

export class FileTreeService {

    private flatTree : any[];
    private treeObject: NodeModel[];
    private level: any;

    constructor() {
        // given stub
        this.flatTree = fileFixtures;
        // multiDimension tree
        this.treeObject = [];
        // level for reduce
        this.level = { treeObject: this.treeObject }
    }

    getFileTree = () : NodeModel => {
        this.flatTree.forEach(file => {
            const path = file.path.split('/');
            path.reduce((r: any, name: string) => {
                if (name === '') name = 'root';
                if (!r[name]) {
                    r[name] = { treeObject: [] };
                    r.treeObject.push( new NodeModel(
                        name,
                        r[name].treeObject,
                        this.isFile(name) ? file as FileModel : null
                    ));
                }
                return r[name];
            }, this.level);
        });
        return this.treeObject[0];
    }

    isFile = (file: string): boolean => file.includes('.');
}