import { Subject } from 'rxjs';
import { FileModel } from '../models/file.model'

const fileSubject = new Subject();

export const fileService = {
    sendFile:(file: FileModel) => fileSubject.next(file),
    getFile:() => fileSubject.asObservable(),
    clearFile:() => fileSubject.next(),
};