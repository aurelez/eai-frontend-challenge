ElementAI frontend challenge
### Services
* `/src/services/filetree.service.ts`
Contient la logigue de conversion de la **flatTree** (stub fourni) en arbre multidimentionel **treeObject**. La génération se fait à l'aide de la méthode **Array.reduce()** de javascript qui prend en entrée un accumulateur et la valeur courrante. À chaque itération, on crée un noeud constitué d'un nom et de la liste de ses noeuds enfants pour la prochiane itétérration ainsi de suite jusqu'au dernier élément.

* `/src/services/file.service.ts`
Utilise l'observable **Subject** de **rxjs** pour passer un fichier (Objet file) du composant **fileTree** au composant **fileContent** qui souscrit au Subject et met à jour son state à chaque émission d'une nouvelle valeur.

### Models
* `/src/models/file.model.ts`
Définit la structure d'un objet file

* `/src/models/node.model.ts`
Définit la structure d'un noeud. Un noeud est un objet récursif, il possède un attribut  **children** de type liste de noeuds.

### Components
* `/src/components/App/app.component.tsx`
Encapsule les composants **FileTreeComponent** et **FileContentComponent**. Il est le point d'entré pour la l'objet **treeObject** (noeud) passé en propriété au composant **FileTreeComponent** .

* `/src/components/FileTree/fileTree.component.tsx`
Conmposant récursif de pésentation dune branche de l'arbre: Il passe son attribut **children** en props. à un nouveau composant **FileTreeComponent** pour construire la prochaine branche, ainsi de suite.

* `/src/components/FileContent/fileContent.component.tsx`
Composant de présentation d'un fichier. Souscrit à l'observable **fileSubject** pour mettre à jour son state lorsqu'un nouvel objet **file** est émis.